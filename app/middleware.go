package app

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"medods_task/config"
)

func InitAuth() (echojwt.Config, echojwt.Config) {
	accessConfig := echojwt.Config{
		ParseTokenFunc: getTokenParseFunc("access", KeyFunc),
		TokenLookup:    "cookie:access-token",
	}
	refreshConfig := echojwt.Config{
		ParseTokenFunc: getTokenParseFunc("refresh", KeyFunc),
		TokenLookup:    "cookie:refresh-token",
	}

	return accessConfig, refreshConfig
}
func getTokenParseFunc(tokenType string, keyFunc func(token *jwt.Token) (interface{}, error)) func(c echo.Context, auth string) (interface{}, error) {
	return func(c echo.Context, auth string) (interface{}, error) {
		token, err := jwt.ParseWithClaims(auth, new(JwtUserClaims), keyFunc)
		if err != nil {
			return nil, &echojwt.TokenError{Token: token, Err: err}
		}
		claims := token.Claims.(*JwtUserClaims)
		if !token.Valid || claims.TokenType != tokenType {
			return nil, &echojwt.TokenError{Token: token, Err: errors.New("invalid token")}
		}
		return token, nil
	}
}

func KeyFunc(token *jwt.Token) (interface{}, error) {
	conf := config.GetConfig()
	if token.Method.Alg() != "HS512" {
		return nil, &echojwt.TokenError{Token: token, Err: fmt.Errorf("unexpected jwt signing method=%v", token.Header["alg"])}
	}
	return []byte(conf.Auth.Secret), nil
}
