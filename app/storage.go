package app

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
	"medods_task/config"
)

type AuthStorage struct {
	c *mongo.Collection
}

func NewAuthStorage(client *mongo.Client) *AuthStorage {
	conf := config.GetConfig()
	store := &AuthStorage{
		c: client.Database(conf.Db.Name).Collection("RefreshToken"),
	}
	return store
}

func (s AuthStorage) InsertRefreshToken(ctx context.Context, token *RefreshToken) error {
	err := crypt(token)
	if err != nil {
		return err
	}
	_, err = s.c.InsertOne(ctx, token)
	return err
}

func (s AuthStorage) FindByID(ctx context.Context, id uuid.UUID) (*RefreshToken, error) {
	filter := bson.D{{"_id", id}}
	var token RefreshToken
	err := s.c.FindOne(ctx, filter).Decode(&token)
	switch {
	case err == nil:
		return &token, nil
	case errors.Is(err, mongo.ErrNoDocuments):
		return nil, ErrNotFound
	default:
		return nil, err
	}
}

func (s AuthStorage) Update(ctx context.Context, token *RefreshToken) error {
	filter := bson.D{{"_id", token.ID}}
	err := crypt(token)
	if err != nil {
		return err
	}
	result, err := s.c.ReplaceOne(ctx, filter, token)
	if err != nil {
		return err
	}
	if result.MatchedCount == 0 {
		return ErrNotFound
	}
	return nil
}

func crypt(token *RefreshToken) error {
	tokenCrypt, err := bcrypt.GenerateFromPassword([]byte(token.Token), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	token.Token = string(tokenCrypt)
	return nil
}
