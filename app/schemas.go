package app

import (
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

type UserClaims struct {
	ID uuid.UUID `json:"id"`
}

type JwtUserClaims struct {
	UserClaims
	TokenType string `json:"tokenType"`
	jwt.RegisteredClaims
}

type RefreshToken struct {
	ID    uuid.UUID `bson:"_id,required"`
	Token string    `bson:"token,required"`
}
