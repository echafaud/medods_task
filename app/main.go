package app

import (
	"context"
	"fmt"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"medods_task/config"
)

func Init(ctx context.Context) error {
	conf, err := config.InitConfig()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s:%s",
		conf.Db.User,
		conf.Db.Password,
		conf.Db.Host,
		conf.Db.Port,
	)))
	if err != nil {
		return err
	}

	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			log.Print(err)
		}
	}()

	authStorage := NewAuthStorage(client)
	authService := NewAuthService(authStorage)

	app := echo.New()
	SetupEndpoints(app, authService)
	return app.Start(":1323")
}

func SetupEndpoints(app *echo.Echo, service *AuthService) {
	_, refresh := InitAuth()
	api := app.Group("/api/v1")
	auth := api.Group("/auth")
	auth.GET("/tokens/:id", service.Tokens)
	auth.POST("/tokens/refresh", service.Refresh, echojwt.WithConfig(refresh))
}
