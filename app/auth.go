package app

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"medods_task/config"
	"net/http"
	"time"
)

type AuthService struct {
	storage *AuthStorage
}

func NewAuthService(authStorage *AuthStorage) *AuthService {
	return &AuthService{
		storage: authStorage,
	}
}

func (s AuthService) Tokens(c echo.Context) error {
	// Если метод используется напрямую с клиентом пользователя, то следует осуществлять проверку на возможность выдачы токенов пользователю,
	// а также возвращать одну и ту же ошибку, например http.StatusUnauthorized, fmt.Errorf("access denied").Error()
	id := c.Param("id")
	userID, err := uuid.Parse(id)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, fmt.Errorf("invalid id").Error())
	}
	conf := config.GetConfig()
	tokenID := uuid.New()
	userClaims := UserClaims{ID: userID} // В условии это не было написано, но я бы проверял существование пользователя в бд
	accessToken, err := s.JwtToken(userClaims, "access", tokenID, time.Minute*time.Duration(conf.Auth.AccessExp))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("error creating the token").Error())
	}
	refreshToken, err := s.JwtToken(userClaims, "refresh", tokenID, time.Hour*24*time.Duration(conf.Auth.AccessExp))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("error creating the token").Error())
	}
	ctx := c.Request().Context()
	_, err = s.storage.FindByID(ctx, userID)
	if errors.Is(err, ErrNotFound) {
		err = s.storage.InsertRefreshToken(c.Request().Context(), &RefreshToken{ID: userID, Token: tokenID.String()})
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("error creating the token in db").Error())
		}
	} else {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("error creating the token in db").Error()) // допускаю создание единственной сессии, в противном случае необходимо разрешить создание и построить логику хранения refresh токена немного иначе
	}

	s.setCookie(c, conf.Auth.AccessCookie, accessToken, true)
	s.setCookie(c, conf.Auth.RefreshCookie, refreshToken, true)
	return c.JSON(http.StatusOK, echo.Map{
		"access_token":  accessToken,
		"refresh_token": refreshToken,
	})
}

func (s AuthService) Refresh(c echo.Context) error {
	conf := config.GetConfig()
	accessCookie, err := c.Cookie(conf.Auth.AccessCookie)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, fmt.Errorf("missing access cookie").Error())
	}
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*JwtUserClaims)

	accessJWT, err := jwt.ParseWithClaims(accessCookie.Value, new(JwtUserClaims), KeyFunc)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("access denied").Error())
	}

	accessClaims := accessJWT.Claims.(*JwtUserClaims)

	if accessClaims.RegisteredClaims.ID != claims.RegisteredClaims.ID {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("access denied").Error())
	}

	token, err := s.storage.FindByID(c.Request().Context(), claims.UserClaims.ID)
	if err != nil {
		bcrypt.CompareHashAndPassword([]byte(claims.RegisteredClaims.ID), []byte(claims.RegisteredClaims.ID))
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("access denied").Error())
	}

	if bcrypt.CompareHashAndPassword([]byte(token.Token), []byte(claims.RegisteredClaims.ID)) != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("access denied").Error())
	}

	tokenID := uuid.New()
	accessToken, err := s.JwtToken(claims.UserClaims, "access", tokenID, time.Minute*time.Duration(conf.Auth.AccessExp))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("access denied").Error())
	}
	refreshToken, err := s.JwtToken(claims.UserClaims, "refresh", tokenID, time.Hour*24*time.Duration(conf.Auth.AccessExp))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("access denied").Error())
	}
	token.Token = tokenID.String()
	err = s.storage.Update(c.Request().Context(), token)
	if err != nil {
		return err
	}
	s.setCookie(c, conf.Auth.AccessCookie, accessToken, true)
	s.setCookie(c, conf.Auth.RefreshCookie, refreshToken, true)
	return c.JSON(http.StatusOK, echo.Map{
		"access_token":  accessToken,
		"refresh_token": refreshToken,
	})
}

func (s AuthService) JwtToken(userClaims UserClaims, tokenType string, tokenID uuid.UUID, exp time.Duration) (string, error) {
	claims := &JwtUserClaims{
		UserClaims: userClaims,
		TokenType:  tokenType,
		RegisteredClaims: jwt.RegisteredClaims{
			ID:        tokenID.String(),
			Subject:   userClaims.ID.String(),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(exp)),
			NotBefore: jwt.NewNumericDate(time.Now()),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		}}
	jwtClaims := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)

	conf := config.GetConfig()
	token, err := jwtClaims.SignedString([]byte(conf.Auth.Secret))
	if err != nil {
		return "", err
	}
	return token, nil
}

func (s AuthService) setCookie(c echo.Context, name string, value string, httpOnly bool) {
	cookie := new(http.Cookie)
	cookie.Name = name
	cookie.Value = value
	cookie.Path = "/"
	cookie.HttpOnly = httpOnly

	c.SetCookie(cookie)
}
