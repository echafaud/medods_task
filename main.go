package main

import (
	"context"
	"log"
	"medods_task/app"
)

func main() {
	ctx := context.Background()
	err := app.Init(ctx)
	if err != nil {
		log.Fatalf("failed to up app: %v", err)
	}
}
