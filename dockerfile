FROM golang:latest

RUN mkdir /medods_task

WORKDIR /medods_task

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o main .

ENTRYPOINT /medods_task/main