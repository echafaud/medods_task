package config

import (
	"github.com/spf13/viper"
)

type DbConfig struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	Name     string `mapstructure:"name"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
}

type AuthConfig struct {
	AccessCookie  string `mapstructure:"accessCookie"`
	RefreshCookie string `mapstructure:"refreshCookie"`
	AccessExp     int    `mapstructure:"accessExp"`
	RefreshExp    int    `mapstructure:"refreshExp"`
	Secret        string `mapstructure:"secret"`
}

type Config struct {
	Db   DbConfig   `mapstructure:"db"`
	Auth AuthConfig `mapstructure:"auth"`
}

var vp *viper.Viper
var config Config

func InitConfig() (Config, error) {
	vp = viper.New()

	vp.SetConfigName("config")
	vp.SetConfigType("json")
	vp.AddConfigPath("./config")
	vp.AddConfigPath(".")
	err := vp.ReadInConfig()

	if err != nil {
		return Config{}, err
	}

	err = vp.Unmarshal(&config)
	if err != nil {
		return Config{}, err
	}

	return config, nil
}

func GetConfig() Config {
	return config
}
